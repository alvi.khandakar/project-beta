import React from "react"
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';



function AutoList(props) {
  const [autos, setAutos] = useState([]);

  useEffect( () => {
    fetch("http://localhost:8100/api/automobiles/")
      .then(response => response.json())
      .then(data => {setAutos (data.autos);
      })
      .catch( e => console.log("Error: ", e));
  }, [])

  const onDeleteAutoClick = (auto) => {
    const autoHref = auto.href;
    const hrefComponents = auto.href.split("/");
    const pk = hrefComponents[hrefComponents.length - 2];
    const autosUrl = `http://localhost:8100/api/automobiles/${pk}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(autosUrl, fetchConfig)
      .then(response => response.json())
      .then( data => {
        if (data.Deleted) {
          const currentAutos = [...autos];
          setAutos(currentAutos.filter( auto => auto.href !== autoHref));
        }
      })
      .catch(e => console.log("Error: ", e));
  }


  return (
    <>
    <div><p></p></div>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<h1>Vehicle Inventory</h1>
			<Link to="/inventory/new">
      	<button className="btn btn-primary btn-lg px-4 gap-3">Add to the Inventory</button>
			</Link>
    </div>
    <table className= "table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
					<th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {autos.map( auto => {
          return (
            <tr key={auto.href}> 
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.model.name}</td>
              <td>{auto.model.manufacturer.name}</td>
              <td>
                <button onClick={() => onDeleteAutoClick(auto)}>X</button>
              </td>

            </tr>
          )
        })}
      </tbody>
    </table>
    </>
  )
}

export default AutoList