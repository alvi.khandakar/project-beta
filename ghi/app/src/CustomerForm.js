import React, { useState } from 'react';

const CustomerForm = () => {

    const [customerName, setName] = useState("");
    const [customerAddress, setAddress] = useState("");
    const [customerPhone, setPhone] = useState("");


    const handleSubmit = async (event) => {
        event.preventDefault();
        const newCustomer = {
            'name': customerName,
            'address': customerAddress,
            'phone': customerPhone,
            
        }

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(newCustomer),
            headers: { 'Content-Type': 'application/json', },
        };
        fetch(customerUrl, fetchConfig)
            .then(response => response.json())
            .then( () => {
                setName('');
                setAddress('');
                setPhone('');
                
            })
            .catch(e => console.log("Error: ", e));
    }

    
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }
    const handlePhoneChange = (event) => {
        const value = event.target.value;
        setPhone(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
                        <div className="form-floating mb-3">
                            <input value={customerName} onChange={handleNameChange} required type="text" name="customer name" id="customer_name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customerAddress} onChange={handleAddressChange} required type="text" name="customer address" id="customer_address" className="form-control" />
                            <label>Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customerPhone} onChange={handlePhoneChange} required type="text" name="customer phone" id="customer_phone" className="form-control" />
                            <label>Phone</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;