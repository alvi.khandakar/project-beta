import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutoList from './AutoList';
import AutoForm from './AutoForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import TechnicianList from './TechnicianList';
import NewTechForm from './NewTechnicianForm';
import ServiceAppointmentList from './ListServiceAppointments';
import ServiceForm from './ServiceForm';
import ServiceHistory from './ServiceHistory';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonList from './SalesPersonList';
import SalesRecordForm from './SalesRecordForm';
import SalesRecordList from './SalesRecordList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="inventory">
            <Route index element={<AutoList/>} />
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="technicians/new" element={<NewTechForm />} />
          <Route path="service/" element={<ServiceAppointmentList />} />
          <Route path="service/new/" element={<ServiceForm />} />
          <Route path="service/history/" element={<ServiceHistory />} />
          <Route path="inventory/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="inventory/manufacturers" element={<ManufacturerList />} />
          <Route path="inventory/models/new" element={<ModelForm />} />
          <Route path="inventory/models/" element={<ModelList />} />
          <Route path="salesperson/new" element={<SalesPersonForm />} />
          <Route path="salespeople" element={<SalesPersonList />} />
          <Route path="customer/new" element={<CustomerForm />} />
          <Route path="customers" element={<CustomerList />} />
          <Route path="salesrecord/new" element={<SalesRecordForm />} />
          <Route path="salesrecords" element={<SalesRecordList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;







