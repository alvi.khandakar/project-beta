import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';

const SalesRecordList = () => {
    const [salesPeople, setSalesPeople] = useState([]);
    const [salesRecord, setSalesRecord] = useState([]);
    // const [selectedEmployee, setSelectedEmployee] = useState('');

    useEffect(() => {
        async function fetchSalesPeople() {
            const url = "http://localhost:8090/api/salespeople/";
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSalesPeople(data.sales_people);
            }
        }
        fetchSalesPeople();
    }, []);
    const handleChange = async (event) => {
        event.preventDefault();
        const personId = event.target.value;
        // setSelectedEmployee(salesPeople.find(salesPerson => salesPerson.id === personId));
        const salesRecordUrl = `http://localhost:8090/api/salespeople/${personId}/salesrecords/`;
        const response = await fetch(salesRecordUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesRecord(data);
        }
    };

    return (
        <>
            <div><p></p></div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <h1>Sales Records</h1>
                <Link to="/salesrecord/new">
                    <button className="btn btn-primary btn-lg px-4 gap-3">Add a Sales Record</button>
                </Link>
            </div>
            <select onChange={handleChange} id="sales-person-sales-record" name="Sales Person" className="form-select mb-3">
                <option value="">Choose a Sales Employee</option>
                {salesPeople.map((salesPerson) => {
                    return (
                        <option key={salesPerson.id} value={salesPerson.id}>
                            {salesPerson.name}
                        </option>
                    );
                })}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Employee</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesRecord.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price.toLocaleString()}.00</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}
export default SalesRecordList;