import React from "react"
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';


function ServiceAppointmentList(props) {
  const [service_appointments, setAppointments] = useState([]);
  const [is_finished, setFinish] = useState(false);

  useEffect( () => {
    fetch("http://localhost:8080/api/appointments/")
      .then(response => response.json())
      .then(data => {setAppointments (data.service_appointments);
      })
      .catch( e => console.log("Error: ", e));
  }, [])

  useEffect( () => {
    fetch("http://localhost:8080/api/appointments/")
      .then(response => response.json())
      .then(data => {setFinish (data.service_appointments.is_finished);
      })
      .catch( e => console.log("Error: ", e));
  }, [])

  const handleDelete = (appointment) => {
    const appointmentHref = appointment.href;
    const hrefComponents = appointment.href.split("/");
    const pk = hrefComponents[hrefComponents.length - 2];
    const appointmentsUrl = `http://localhost:8080/api/appointments/${pk}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(appointmentsUrl, fetchConfig)
      .then(response => response.json())
      .then( data => {
        if (data.Deleted) {
          const currentAppointments = [...service_appointments];
          setAppointments(currentAppointments.filter( appointment => appointment.href!== appointmentHref));
        }
      })
      .catch(e => console.log("Error: ", e));
  }

  const handleFinishChange = (appointment) => {
    setFinish( current => !current);
    const finishStatus = {
      "is_finished": {is_finished}
    }
    
    const appointmentHref = appointment.href;
    const hrefComponents = appointment.href.split("/");
    const pk = hrefComponents[hrefComponents.length - 2];
    const appointmentsUrl = `http://localhost:8080/api/appointments/${pk}/`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(finishStatus),
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(appointmentsUrl, fetchConfig)
    .then(response => response.json())
    .then(setAppointments(service_appointments.filter(appointment => appointment.href!== appointmentHref)))
    }
  


  return (
    <>
    <div><p></p></div>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<h1>Service Appointments</h1>
			<Link to="/service/new">
      	<button className="btn btn-primary btn-lg px-4 gap-3">Add a New Appointment</button>
			</Link>
    </div>
    <table className= "table table-striped">
      <thead>
        <tr>
          <th>Vin</th>
          <th>Customer Name</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Options</th>
          <th>Job Status</th>
        </tr>
      </thead>
      <tbody>
        {service_appointments.map( appointment => {
          return (
            <tr key={appointment.href}> 
              <td>{appointment.vin}</td>
              <td>{appointment.customer_name}</td>
              <td>{appointment.date}</td>
              <td>{appointment.time}</td>
              <td>{appointment.technician.tech_name}</td>
              <td>{appointment.reason}</td>
              <td>
                <form>
                  <button type="button" className="btn btn-danger"
                    onClick={() => handleDelete(appointment)}>{service_appointments ? 'Cancel' : 'Delete item'}</button>
                </form>
              </td>
              <td>
                <form>
                  <button type="button" className="btn btn-success"
                  onClick={ () => handleFinishChange(appointment)}>Finish</button>
                </form>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
    </>
  )
}

export default ServiceAppointmentList