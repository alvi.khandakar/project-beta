import React from "react"
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';


function TechnicianList(props) {
  const [technicians, setTechs] = useState([]);

  useEffect( () => {
    fetch("http://localhost:8080/api/technicians/")
      .then(response => response.json())
      .then(data => {setTechs (data.technicians);
      })
      .catch( e => console.log("Error: ", e));
  }, [])

  const onDeleteTechClick = (tech) => {
    const techHref = tech.href;
    const hrefComponents = tech.href.split("/");
    const pk = hrefComponents[hrefComponents.length - 2];
    const techsUrl = `http://localhost:8080/api/technicians/${pk}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(techsUrl, fetchConfig)
      .then(response => response.json())
      .then( data => {
        if (data.Deleted) {
          const currentTechs = [...technicians];
          setTechs(currentTechs.filter( tech => tech.href !== techHref));
        }
      })
      .catch(e => console.log("Error: ", e));
  }


  return (
    <>
    <div><p></p></div>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<h1>Technician Staff</h1>
			<Link to="/technicians/new">
      	<button className="btn btn-primary btn-lg px-4 gap-3">Add a New Technician</button>
			</Link>
    </div>
    <table className= "table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Employee Number</th>
          <th>Remove</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map( tech => {
          return (
            <tr key={tech.href}> 
              <td>{tech.tech_name}</td>
              <td>{tech.employee_number}</td>
              <td>
                <button onClick={() => onDeleteTechClick(tech)}>X</button>
              </td>

            </tr>
          )
        })}
      </tbody>
    </table>
    </>
  )
}

export default TechnicianList