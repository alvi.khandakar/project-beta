import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// async function loadInventory() {
//   let manufacturerData, modelData, autoData
//   const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
//   const modelResponse = await fetch('http://localhost:8100/api/models/');
//   const autoResponse = await fetch('http://localhost:8100/api/automobiles/');

//   if (manufacturerResponse.ok) {
//     manufacturerData = await manufacturerResponse.json();
//   }
//   if (modelResponse.ok) {
//     modelData = await modelResponse.json();
//   }
//   if (autoResponse.ok) {
//     autoData = await autoResponse.json();
//   }


//   root.render(
//     <React.StrictMode>
//       <App 
//       manfuacturers={manufacturerData}
//       models={modelData.models}
//       autos={autoData.autos}
//       />
//     </React.StrictMode>
//   );
// }
// loadInventory();
