import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom";
import './index.css';


const SalesPersonList = () => {
    const [salesPeople, setSalesPeople] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8090/api/salespeople/")
            .then(response => response.json())
            .then(data => setSalesPeople(data.sales_people))
            .catch((e) => console.error("Error: ", e));
    }, []);

    return (
        <>
            <div><p></p></div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <h1>Sales People</h1>
                <Link to="/salesperson/new">
                    <button className="btn btn-primary btn-lg px-4 gap-3">Add a Sales Person</button>
                </Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                    {salesPeople.map(salesPerson => {
                        return (
                            <tr key={salesPerson.id}>
                                <td>{salesPerson.name}</td>
                                <td>{salesPerson.employee_number}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default SalesPersonList;