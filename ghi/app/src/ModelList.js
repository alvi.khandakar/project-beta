import React from "react"
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './index.css';


function ModelList(props) {
  const [models, setModels] = useState([]);

  useEffect( () => {
    fetch("http://localhost:8100/api/models/")
      .then(response => response.json())
      .then(data => {setModels (data.models);
      })
      .catch( e => console.log("Error: ", e));
  }, [])

	/* This section of Code is not used because we shouldn't allow Vehicle Model Deletion... yet?
  const onDeleteModelClick = (model) => {
    const modelHref = model.href;
    const hrefComponents = model.href.split("/");
    const pk = hrefComponents[hrefComponents.length - 2];
    const modelsUrl = `http://localhost:8100/api/models/${pk}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(modelsUrl, fetchConfig)
      .then(response => response.json())
      .then( data => {
        if (data.Deleted) {
          const currentModels = [...models];
          setModels(currentModels.filter( model => model.href !== modelHref));
        }
      })
      .catch(e => console.log("Error: ", e));
  }
  */

  return (
    <>
    <div><p></p></div>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<h1>Vehicle Models</h1>
      <Link to="/inventory/models/new">
      	<button className="btn btn-primary btn-lg px-4 gap-3">Add a Vehicle Model</button>
			</Link>
    </div>
    <table className= "table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Stock Image</th>
        </tr>
      </thead>
      <tbody>
        {models.map( model => {
          return (
            <tr key={model.href}> 
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td>
                <img className = "model-image" src={model.picture_url} alt={model.name}/>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
    </>
  )
}

export default ModelList