import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom";
import './index.css';


const CustomerList = () => {
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8090/api/customers/")
            .then(response => response.json())
            .then(data => setCustomers(data.customers))
            .catch((e) => console.error("Error: ", e));
    }, []);

    return (
        <>
            <div><p></p></div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <h1>Customers</h1>
                <Link to="/customer/new">
                    <button className="btn btn-primary btn-lg px-4 gap-3">Add a Customer</button>
                </Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default CustomerList;