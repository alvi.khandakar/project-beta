from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, ServiceAppointment



class AutomobileVOEncoder(ModelEncoder):
	model = AutomobileVO
	properties = [
		"import_href",
		'vin',
		"id",
	]

class TechnicianEncoder(ModelEncoder):
	model = Technician
	properties = [
		"id",
		'tech_name', 
		'employee_number',
	]

class ServiceAppointmentEncoder(ModelEncoder):
	model = ServiceAppointment
	properties = [
		'id',
		'vin',
		'customer_name',
		'date',
		'time',
		'technician',
		'reason',
		'vip',
		'is_finished',
		'cancelled',
	]
	encoders={'technician': TechnicianEncoder()}

class HistoryEncoder(ModelEncoder):
	model = ServiceAppointment
	properties = [
		"id",
		"vin",
		"customer_name",
		"date",
		'time',
		'technician',
		'reason',
	]
	encoders={'technician': TechnicianEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
	if request.method == "GET":
		technicians = Technician.objects.all()
		return JsonResponse(
			{"technicians": technicians},
			TechnicianEncoder,
			safe=False,
		)
	else: # POST Request
		try:
			content = json.loads(request.body)
			technician = Technician.objects.create(**content)
			return JsonResponse(
				technician,
				encoder=TechnicianEncoder,
				safe=False,
			)
		except:
			return JsonResponse(
				{"message": "Could not create this technician"},
				status=400,
				)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_tech(request, pk):
	if request.method == "GET":
		try:
			technician = Technician.objects.get(id=pk)
			return JsonResponse(
				technician,
				encoder=TechnicianEncoder,
				safe=False,
			)
		except Technician.DoesNotExist:
			return JsonResponse(
				{"Message": "Invalid Technician Employee ID provided."},
				status=400,
			)
	elif request.method == "DELETE":
		count, _ = Technician.objects.filter(id=pk).delete()
		return JsonResponse({"Deleted": count > 0})
	else: # PUT Request
		content = json.loads(request.body)
		Technician.objects.filter(id=pk).update(**content)
		technician = Technician.objects.get(id=pk)
		return JsonResponse(
			technician,
			encoder=TechnicianEncoder,
			safe=False,
		)



@require_http_methods(["GET", "POST"])
def api_list_service_appointments(request):
	if request.method == "GET":
		appointments = ServiceAppointment.objects.all()
		return JsonResponse(
			{"service_appointments": appointments},
			encoder=ServiceAppointmentEncoder,
		)
	else:
		content = json.loads(request.body)
		try:
			check_number = content["technician"]
			technician = Technician.objects.get(employee_number=check_number)
			content["technician"] = technician
		except Technician.DoesNotExist:
			return JsonResponse(
				{"message" : "Invalid Employee Number"},
				status=400,
			)
		if hasattr(content, "vip"):
			del content["vip"]
		if hasattr(content, "is_finished"):
			del content["is_finished"]

		appointment = ServiceAppointment.objects.create(**content)
		print(appointment.technician)
		return JsonResponse(
			appointment,
			encoder=ServiceAppointmentEncoder,
			safe=False,
		)

# @require_http_methods(["GET"])
# def api_show_service_history(request, pk):
# 	if request.method == "GET":
# 		history = ServiceAppointment.objects.get(vin=pk)
# 		print(history)
# 		return JsonResponse(
# 			history,
# 			encoder=HistoryEncoder,
# 			safe=False,
# 		)

@require_http_methods(["GET"])
def api_show_service_history(request, pk):
	if request.method == "GET":
		history = ServiceAppointment.objects.filter(vin=pk)
		if history == []:
			return JsonResponse(
					{"message": "Not a valid vin"},
					status=404,
					safe=False,
					)
		else:
			print("Here is that print", history)
			return JsonResponse(
				history,
				encoder=HistoryEncoder,
				safe=False
			)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, pk):
	if request.method == "GET":
		appointment = ServiceAppointment.objects.get(id=pk)
		return JsonResponse(
			appointment,
			encoder=ServiceAppointmentEncoder,
			safe=False,
		)
	elif request.method == "DELETE":
		count, _ = ServiceAppointment.objects.filter(id=pk).delete()
		return JsonResponse({"Deleted": count > 0})
	else: # PUT Request
		try:
				content = json.loads(request.body)
				appointment = ServiceAppointment.objects.get(id=pk)
				if "technician" in content:
						try:
								technician = Technician.objects.get(
										employee_number=content["technician"]
								)
								setattr(appointment, "technician", technician)
						except Technician.DoesNotExist:
								return JsonResponse(
										{"message": "Invalid Technician employee number provided."},
										status=400,
								)
				props = ["customer_name", "date", "time", "reason"]
				for prop in props:
						if prop in content:
								setattr(appointment, prop, content[prop])
				appointment.save()

				return JsonResponse(
						appointment,
						encoder=ServiceAppointmentEncoder,
						safe=False,
				)
		except ServiceAppointment.DoesNotExist:
				response = JsonResponse({"message": "Service appointment does not exist"})
				response.status_code = 404
				return response
