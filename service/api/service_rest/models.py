from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
  import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
  vin = models.CharField(max_length=17, unique=True)
  color = models.CharField(max_length=50)
  year = models.PositiveSmallIntegerField(blank=False)
  model_name = models.CharField(max_length=100)

  def __str__(self):
    return f"{self.year} {self.model_name} {self.color}"

  class Meta:
      verbose_name_plural = "Automobile VOs"



class Technician(models.Model):
  tech_name = models.CharField(max_length=100)
  employee_number = models.PositiveIntegerField(unique=True)

  def get_api_url(self):
    return reverse("api_show_tech", kwargs={"pk": self.pk})

  def __str__(self):
    return self.tech_name


class ServiceAppointment(models.Model):
  vin = models.CharField(max_length=17)
  customer_name = models.CharField(max_length=100)
  date = models.DateField(null=True)
  time = models.TimeField(null=True)
  reason = models.CharField(max_length=200)
  vip = models.BooleanField(default=False)
  is_finished = models.BooleanField(default=False)
  cancelled = models.BooleanField(default=False)
  technician = models.ForeignKey(
    Technician,
    related_name="services",
    on_delete=models.PROTECT,
  )

  def get_api_url(self):
    return reverse("api_show_appointment", kwargs={"pk": self.pk})

  def __str__(self):
    return f"{self.customer_name}'s service appointment. VIP Status = {self.vip}"

  def change_finish_status(self):
    self.is_finished = not self.is_finished
    self.save()

  def change_appointment_status(self):
    self.cancelled = not self.cancelled
    self.save()

  def set_vip_status(self, *args, **kwargs):
    dealership_car = AutomobileVO.objects.filter(vin=self.vin)
    self.vip = len(dealership_car) > 0
    super().save(*args, **kwargs)
    
  class Meta:
    ordering = ("date", "customer_name", "reason")
