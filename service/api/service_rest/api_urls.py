from django.urls import path

from .api_views import (
    api_list_service_appointments,
    api_show_appointment,
    api_list_technicians,
    api_show_tech,
    api_show_service_history,
)

urlpatterns = [
    path("appointments/", api_list_service_appointments, name="api_list_service_appointments"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_show_tech, name="api_show_tech"),
    path("servicehistory/vin/<str:pk>/", api_show_service_history, name="api_show_service_history"),
]
