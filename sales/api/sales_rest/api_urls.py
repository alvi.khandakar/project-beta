from django.urls import path
from .api_views import api_sales_person_list, api_customer_list, api_sales_record_list, api_sales_record_detail

urlpatterns = [
    path('salespeople/', api_sales_person_list, name="api_sales_person"),
    path('customers/', api_customer_list, name="api_customer"),
    path('salesrecords/', api_sales_record_list, name="api_sales_record"),
    path('salespeople/<int:id>/salesrecords/', api_sales_record_detail, name="api_sales_record_detail")
]

